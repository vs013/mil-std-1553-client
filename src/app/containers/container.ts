import { Component } from '@angular/core';

@Component({
    selector: 'main-container',
    template: `
    <div>
        <main class='main'>
        Main content here
        </main>
    </div>
    `
})
export class MainContainerComponent {};
