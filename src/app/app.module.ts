import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './components/app.component';
import { MainContainerComponent, NotesContainerComponent } from './containers';
import { AppBarComponent, NoteCardComponent } from './ui';

@NgModule({
  imports:      [ BrowserModule ],
  declarations: [
    AppComponent,
    MainContainerComponent,
    NotesContainerComponent,
    AppBarComponent,
    NoteCardComponent,
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
